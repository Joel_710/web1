<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Restaurante UTC</title>
    <!--Importacion de Jquery v3-->
    <script src="https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
    <!--Importacion de bs CDN-->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">                                                
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!--Importacion Api-->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjjZeBI_UzLSXMRFR19BL4po4Z1cyKm1g&libraries=places&callback=initMap"></script> 
</head>

<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo site_url();?>/welcome">
                RESTAURANTE UTC</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active">
                <a href="<?php echo site_url()?>/menus/desayunos">DESAYUNOS<span class="sr-only">(current)</span></a>
                </li>
                <li><a href="<?php echo site_url()?>/menus/almuerzos">ALMUERZOS</a></li>
                <li><a href="<?php echo site_url()?>/menus/meriendas">MERIENDAS</a></li>
                <li><a href="<?php echo site_url()?>/menus/platos_a_la_carta">CARTA</a></li>
            </ul>
            <form class="navbar-form navbar-left">
                <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
            
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="row">
        <div class="col-md-12 text-center">
        <img width="100%"; src="<?php echo base_url()?>
        /assets/images/utc.png" 
        alt="">

            <h1> <b>BIENVENIDOS
            <br>RESTAURANTE UTC</b> </h1>
            
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2> <b>Mision</b></h2>
                <p>Nuestra misión como restaurante universitario es proporcionar a la comunidad estudiantil y académica un ambiente acogedor y saludable donde puedan disfrutar de comidas nutritivas y equilibradas a precios asequibles. Buscamos fomentar hábitos alimenticios saludables y promover el bienestar general de nuestros comensales, brindando opciones de comida variada y de calidad que satisfagan sus necesidades dietéticas y preferencias.
                </p>
            </div>

            <div class="col-md-6">
                <h2> <b>Vision</b></h2>
                <p>Nuestra visión es convertirnos en el restaurante universitario de referencia, reconocido por nuestra excelencia en la alimentación y servicios. Aspiramos a ser un espacio donde la comunidad universitaria se reúna no solo para satisfacer sus necesidades nutricionales, sino también para socializar y compartir experiencias. Queremos ser líderes en la promoción de una alimentación saludable y sostenible, colaborando con proveedores locales y adoptando prácticas amigables con el medio ambiente en nuestras operaciones.
                </p>
            </div>

        </div>

    </div>
    
    <div class="container">
        <h2><b>Ubicanos en:</b></h2>
        <div class="row">
            <div class="col-md-4">
                <table class="table table-bordered table-striped table-hover">
                    <br>
                    <tr>
                        <th class="text-right">DIRECIÓN:</th>
                        <td>39J8+RRJ, Av. Simón Rodríguez, Latacunga</td>
                    </tr>
                    <tr>
                        <th class="text-right">HORARIO DE ATENCIÓN:</th>
                        <td>Abre a las 07:30 am y cierra a las 09:00 pm</td>
                    </tr>
                    <tr>
                        <th class="text-right">TELÉFONO:</th>
                        <td>(03) 281-0296</td>
                    </tr>
                    <tr>
                        <th class="text-right">UBICAIÓN:</th>
                        <td>LATACUNGA-ECUADOR</td>
                    </tr>
                    <tr>
                        <th class="text-right">EMAIL:</th>
                        <td>restaurante.utc@gmail.com</td>
                    </tr>
                    
                </table>
            </div>
                <div class="col-md-7">
                    <div id="mapaDireccion" style="100%; height:350px;"></div>
                </div>
        </div>
        
        <script type="text/javascript">
            function initMap(){
                var coordenadaCentral=
                new google.maps.LatLng(-0.9155596917144397, -78.63296837433572);
                var mapa1=new google.maps.Map(document.getElementById("mapaDireccion"),
                {
                    center:coordenadaCentral,
                    zoom:15,
                    mapTypeId:google.maps.MapTypeId.ROADMAP
                }
                );
            }
        </script>

    </div>
    <br>
    <br>


    <div class="row"
    style="background-color:black; color:white;padding:10px;">
        <div class="col-md-6 text-center">
            <h4>
                <b>UTC&copy;</b>
            </h4>
        </div>
        <br>
        <div class="col-md-6 text-center">
            DESARROLLADO POR: <hr>
            JOEL MENA
        </div>
    </div>

</body>
</html>

