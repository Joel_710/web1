    <div class="container">
        <div class="row">   
            <div class="col-md-12 text-center">
                <h1> <b>DESAYUNOS</b> </h1>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 align="center";>Ven y disfruta de unos fabulosos desayunos te esperamos no faltes.</h2>
                <h4>Muchos de los desayunos típicos ecuatorianos también son deliciosos para un brunch del fin de semana o para el cafecito de la tarde.</h4>
            </div>
        </div>
    </div>
    <br>
    <br>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/a.jpg"  width="240" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Desayuno Aleman</h3>
                    <h5 class="text-center">Capuchino, Chocolate, Cafe</h5>
                    <p class="text-center">$2.50 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/b.jpg"  width="160" height="150" alt="">
                <div class="caption">
                    <h3 class="text-center">Bolones</h3>
                    <h5 class="text-center">Cafe Minerva, Chocolate</h5>
                    <p class="text-center">$1.50 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/e.png"  width="300" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Empanadas Chilenas</h3>
                    <h5 class="text-center">Cafe, Té, Chocolate</h5>
                    <p class="text-center">$3.50 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    </div>
</div>
<br>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/h.png"  width="220" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Humitas</h3>
                    <h5 class="text-center">Cafe, Chocolate, Avena</h5>
                    <p class="text-center">$1.25 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/q.png"  width="340" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Quinbolitos</h3>
                    <h5 class="text-center">Cafe, Chocolate</h5>
                    <p class="text-center">$1.70 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/ch.png"  width="100%" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Churros con chocolate</h3>
                    <h5 class="text-center">Chocolate espeso</h5>
                    <p class="text-center">$2.50 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    </div>
</div>
<br>
