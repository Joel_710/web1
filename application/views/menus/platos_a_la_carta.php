<div class="container">
        <div class="row">   
            <div class="col-md-12 text-center">
                <h1> <b>PLATOS A LA CARTA</b> </h1>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 align="center";>Ven y disfruta de unos fabulosos platos a la carta te esperamos no faltes.</h2>
                <h4></h4>
            </div>
        </div>
    </div>
    <br>
    <br>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/fritada.jpg"  width="170" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Fritada</h3>
                    <p class="text-center">$2.50 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/hornado.jpg"  width="248" height="150" alt="">
                <div class="caption">
                    <h3 class="text-center">Hornado</h3>
                    <p class="text-center">$2.25 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/parrillada.jpg"  width="270" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Parrillada</h3>
                    <p class="text-center">$2.99 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    </div>
</div>
<br>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/chonta.jpg"  width="320" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Chontacuros</h3>
                    <p class="text-center">$2.99 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/yahu.jpg"  width="240" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Yahuarlocro</h3>
                    <p class="text-center">$2.75 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/llapi.jpg"  width="138" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Llapingacho</h3>
                    <p class="text-center">$2.50 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    </div>
</div>
<br>