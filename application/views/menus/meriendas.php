<div class="container">
        <div class="row">   
            <div class="col-md-12 text-center">
                <h1> <b>MERIENDAS</b> </h1>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 align="center";>Ven y disfruta de unos fabulosos almuerzos te esperamos no faltes.</h2>
                <h4></h4>
            </div>
        </div>
    </div>
    <br>
    <br>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/camarones.jpg"  width="275" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Seco de Camaron</h3>
                    <p class="text-center">$2.50 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/chaula.jpg"  width="300" height="150" alt="">
                <div class="caption">
                    <h3 class="text-center">Chaulafan</h3>
                    <p class="text-center">$2.25 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/encocado.jpg"  width="330" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Encocado de pecado</h3>
                    <p class="text-center">$2.99 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    </div>
</div>
<br>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/aguado.jpg"  width="320" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Aguado de Pollo</h3>
                    <p class="text-center">$2 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/res.jpg"  width="285" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Sancocho de res</h3>
                    <p class="text-center">$2.75 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/biche.jpg"  width="310" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Caldo de biche</h3>
                    <p class="text-center">$2.50 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    </div>
</div>
<br>