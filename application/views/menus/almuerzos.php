<div class="container">
        <div class="row">   
            <div class="col-md-12 text-center">
                <h1> <b>ALMUERZOS</b> </h1>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 align="center";>Ven y disfruta de unos fabulosos almuerzos te esperamos no faltes.</h2>
                <h4></h4>
            </div>
        </div>
    </div>
    <br>
    <br>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/chancho.jpg"  width="300" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Seco de Chancho</h3>
                    <p class="text-center">$2.50 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/chivo.jpg"  width="300" height="150" alt="">
                <div class="caption">
                    <h3 class="text-center">Seco de Chivo</h3>
                    <p class="text-center">$3.50 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/seco.jpg"  width="250" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Seco de pollo</h3>
                    <p class="text-center">$2.50 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    </div>
</div>
<br>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/menestra.jpg"  width="320" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Menestra con carne asada</h3>
                    <p class="text-center">$2.25 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    
        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/pollo.jpg"  width="350" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Aguado de pollo</h3>
                    <p class="text-center">$2.70 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="thumbnail">
                <img src="<?php echo base_url();?>/assets/images/guatita.jpg"  width="260" height="300" alt="">
                <div class="caption">
                    <h3 class="text-center">Guatita</h3>
                    <p class="text-center">$2.50 </p>
                    <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
                </div>
            </div>
        </div>

    </div>
</div>
<br>