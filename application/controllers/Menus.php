<?php 
    class Menus extends CI_Controller
    {
        //Constructor
        function __construct()
        {
           parent::__construct(); 
        }
        //Renderizacion de la vista que 
        //muestra los desayunos
        
        public function welcome_message(){
            $this->load->view('header');
            $this->load->view('/welcome');
            $this->load->view('footer');
        }

        public function desayunos(){
            $this->load->view('header');
            $this->load->view('menus/desayunos');
            $this->load->view('footer');
        }

        public function almuerzos(){
            $this->load->view('header');
            $this->load->view('menus/almuerzos');
            $this->load->view('footer');
        }

        public function meriendas(){
            $this->load->view('header');
            $this->load->view('menus/meriendas');
            $this->load->view('footer');
        }

        public function platos_a_la_carta(){
            $this->load->view('header');
            $this->load->view('menus/platos_a_la_carta');
            $this->load->view('footer');
        }


    } //No borar no sea pendejo 
?>